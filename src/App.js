import { gUserInfo, showAgeInfo } from "./info";

function App() {
  return (
    <div className="App">
      <h4>Họ tên: {gUserInfo.firstname} {gUserInfo.lastname}</h4>
      <img src={gUserInfo.avatar} width="300px"/>
      <p>Tuổi: {gUserInfo.age} <br/>
        ({showAgeInfo()})
      </p>
      <ul>
        {gUserInfo.language.map((value, i)=>{
          return <li key={i}>{value}</li>
        })}
      </ul>
    </div>
  );
}

export default App;
