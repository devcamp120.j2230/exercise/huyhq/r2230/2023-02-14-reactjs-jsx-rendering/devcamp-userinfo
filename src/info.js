import avatar from "./src/asset/image/avatardefault_92824.png";
const gUserInfo = {
  firstname: 'Hoang',
  lastname: 'Pham',
  avatar,
  age: 30,
  language: ['Vietnamese', 'Japanese', 'English']
}

const showAgeInfo = ()=> gUserInfo.age > 35 ? "Anh ấy đã già" : "Anh ấy còn trẻ";

export {gUserInfo, showAgeInfo};